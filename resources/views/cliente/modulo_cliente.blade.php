<link rel="stylesheet" href="../css/cliente.css">
<div id="content_cliente">
    <div class="row">
        <div class="col-md-12">
            <h1>Seccion clientes</h1>
        </div>
    </div>
    {{-- BTN NUEVO CLIENTE --}}
    <div class="row" style="margin-top: 20px; margin-bottom: 40px; width: 100%">
        <div class="col-md-12">
            <button class="btn btn-success" data-toggle="modal" data-target="#crearClienteModal">Nuevo cliente</button>
        </div>
    </div>


    {{-- TABLE CLIENTE --}}
    <div class="row" style="width: 90%">
        <div class="col-md-12">
            <table id="tableClientes">
                <thead>
                    <tr>
                        <th class="encabezado">Nombre</th>
                        <th class="encabezado">Apellido</th>
                        <th class="encabezado">Email</th>
                        <th class="encabezado">Grupo</th>
                        <th class="encabezado">Acciones</th>
                    </tr>
                    {{-- Filtros --}}
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th>
                            <input type="text" id="search_nombre" placeholder="Buscar nombre">
                        </th>
                        <th>
                            <input type="text" id="search_apellido" placeholder="Buscar apellido">
                        </th>
                        <th>
                            <input type="text" id="search_email" placeholder="Buscar email">
                        </th>
                        <th>
                            <select id="search_grupo_cliente"></select>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{-- CREAR CLIENTE MODAL--}}
    <div class="modal fade" id="crearClienteModal" tabindex="-1" role="dialog" aria-labelledby="crearClienteLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="crearClienteLabel">Crear cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nombre</span>
                        </div>
                        <input type="text" class="form-control" aria-label="nombre" id="crear_cliente_nombre"
                            aria-describedby="crear-nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Apellido</span>
                        </div>
                        <input type="text" class="form-control" aria-label="apellido" aria-describedby="crear-apellido"
                            id="crear_cliente_apellido" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Email</span>
                        </div>
                        <input type="text" class="form-control" aria-label="email" aria-describedby="crear-email"
                            id="crear_cliente_email" required>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Observaciones</span>
                        </div>
                        <input type="text" class="form-control" aria-label="observaciones"
                            aria-describedby="crear-observaciones" id="crear_cliente_observaciones">
                    </div>
                    <label for="crear-grupo-cliente" class="lbl-crear-cliente">
                        Grupo
                    </label>
                    <select name="grupo_cliente_id" id="crear_cliente_grupo" required>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnCrearCliente">Crear</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{-- ELIMINAR CLIENTE MODAL CONFIRMACIOn--}}
    <div class="modal fade" id="borrarClienteModal" tabindex="-1" role="dialog" aria-labelledby="eliminarClienteLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="eliminarClienteLabel">Eliminar cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="cliente_id_a_borrar">
                <div class="modal-body">
                    <h4 id="tituloBorarClienteConfirmacion"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnConfirmarBorradoCliente">Confirmar
                        borrado</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{-- EDITAR CLIENTE MODAL --}}
    <div class="modal fade" id="editarClienteModal" tabindex="-1" role="dialog"
        aria-labelledby="editarClienteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <input type="hidden" id="editar_cliente_id">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editarClienteModalLabel">Editar cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nombre</span>
                        </div>
                        <input type="text" class="form-control" aria-label="nombre" id="editar_cliente_nombre"
                            aria-describedby="crear-nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Apellido</span>
                        </div>
                        <input type="text" class="form-control" aria-label="apellido" aria-describedby="crear-apellido"
                            id="editar_cliente_apellido" required>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Email</span>
                        </div>
                        <input type="text" class="form-control" aria-label="email" aria-describedby="crear-email"
                            id="editar_cliente_email" required>
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Observaciones</span>
                        </div>
                        <input type="text" class="form-control" aria-label="observaciones"
                            aria-describedby="crear-observaciones" id="editar_cliente_observaciones">
                    </div>
                    <label for="crear-grupo-cliente" class="lbl-crear-cliente">
                        Grupo
                    </label>
                    <select name="grupo_cliente_id" id="editar_cliente_grupo" required>                    
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnEditarCliente">Guardar cambios</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../js/cliente/cliente.js"></script>
