<link rel="stylesheet" href="../css/cliente.css">

<div id="content_grupo_cliente" style="display: none">
    <div class="row" style="width: 100%;">
        <div class="col-md-12">
            <h1>Seccion Grupo Clientes</h1>
        </div>
    </div>
    {{-- BTN NUEVO GRUPO CLIENTE --}}
    <div class="row" style="margin-top: 20px; margin-bottom: 40px; width: 100%">
        <div class="col-md-12">
            <button class="btn btn-success" data-toggle="modal" data-target="#crearGrupoClienteModal">Nuevo grupo
                cliente</button>
        </div>
    </div>


    {{-- TABLE  GRUPO CLIENTE --}}
    <div class="row" style="width: 90%">
        <div class="col-md-12">
            <table id="tableGruposCliente" style="width: 100%">
                <thead>
                    <tr>
                        <th class="encabezado">Nombre</th>
                        <th class="encabezado">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>

            </table>
        </div>
    </div>

    {{-- CREAR GRUPO CLIENTE MODAL--}}
    <div class="modal fade" id="crearGrupoClienteModal" tabindex="-1" role="dialog"
        aria-labelledby="crearGrupoClienteLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="crearGrupoClienteLabel">Crear grupo cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nombre</span>
                        </div>
                        <input type="text" class="form-control" aria-label="nombre" id="crear_grupo_cliente_nombre"
                            aria-describedby="crear-nombre" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnCrearGrupoCliente">Crear</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{-- ELIMINAR GRUPO CLIENTE MODAL CONFIRMACION--}}
    <div class="modal fade" id="borrarGrupoClienteModal" tabindex="-1" role="dialog"
        aria-labelledby="eliminarGrupoClienteLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="eliminarGrupoClienteLabel">Eliminar grupo cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" id="grupo_cliente_id_a_borrar">
                <div class="modal-body">
                    <h4 id="tituloBorrarGrupoCliente"></h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnConfirmarBorradoGrupoCliente">Confirmar
                        borrado</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{-- EDITAR GRUPO CLIENTE MODAL --}}
    <div class="modal fade" id="editarGrupoClienteModal" tabindex="-1" role="dialog"
        aria-labelledby="editarGrupoClienteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <input type="hidden" id="editar_grupo_cliente_id">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editarGrupoClienteModalLabel">Editar grupo cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Nombre</span>
                        </div>
                        <input type="text" class="form-control" aria-label="nombre" id="editar_grupo_cliente_nombre"
                            aria-describedby="crear-nombre" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnEditarGrupoCliente">Guardar cambios</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="../js/grupo_cliente/grupo_cliente.js"></script>
