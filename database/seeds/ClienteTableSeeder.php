<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Genero 870 clientes con Faker para probar
        $faker = Faker::create();
        foreach (range(1, 250) as $index) {
            DB::table('clientes')->insert([
                'nombre' => $faker->name,
                'apellido' => $faker->lastName,
                'email' => $faker->email,
                'observaciones' => $faker->text,
                'grupo_cliente_id' => rand(1, 4)
            ]);
        }
    }
}
