<?php

use App\GrupoCliente;
use Illuminate\Database\Seeder;

class GrupoClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Genero 4 grupos aleatorios para hacer pruebas
        $data_insert = [];
        $data_insert[] = ['id' => 1, 'nombre' => 'Estandar'];
        $data_insert[] = ['id' => 2, 'nombre' => 'Premium'];
        $data_insert[] = ['id' => 3, 'nombre' => 'VIP'];
        $data_insert[] = ['id' => 4, 'nombre' => 'Poco habituales'];

        GrupoCliente::insert($data_insert);
    }
}
