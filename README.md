## Deploy

1. Clonar el proyecto.
2. Ingresar a la carpeta ejercicio_urbano
3. docker-compose up -d
4. Copiar y pegar el .env.example como .env
5. docker-compose exec app composer install
6. docker-compose exec app php artisan migrate:fresh --seed   //Esto llena la base de datos
7. Ir a: localhost:8000
8. Ante cualquier duda: marzullonicolas@gmail.com
