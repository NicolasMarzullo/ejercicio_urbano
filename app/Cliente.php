<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $fillable = ['nombre', 'apellido', 'email', 'observaciones','grupo_cliente_id'];
    public function grupo()
    {
        return $this->belongsTo('App\GrupoCliente', 'grupo_cliente_id');
    }
}
