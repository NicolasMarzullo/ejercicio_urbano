<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\GrupoCliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GrupoClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(['grupos' => GrupoCliente::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $grupo_cliente = new GrupoCliente();

        $data = $request->all();
        $validator = Validator::make($data, $this->rules(), $this->messages());

        if ($validator->fails())
            return response(["error_msg" => $validator->errors()->first()], 422);

        $grupo_cliente->nombre = $data['nombre'];
        $grupo_cliente->save();
        return response(['grupo' => $grupo_cliente]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $grupo_cliente = GrupoCliente::find($id);
        if (empty($grupo_cliente))
            return response("No se pudo encontrar el grupo cliente solicitado", 400);

        return response($grupo_cliente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $grupo_cliente = GrupoCliente::find($id);
        if (empty($grupo_cliente))
            return response("El grupo cliente que desea actualizar no existe", 404);


        $data = $request->all();
        $validator = Validator::make($data, $this->rules(), $this->messages());

        if ($validator->fails())
            return response(["error_msg" => $validator->errors()->first()], 422);

        $grupo_cliente->nombre = $data['nombre'];
        $grupo_cliente->save();
        return response($grupo_cliente);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //Valido que no existan clientes con este grupo
        $clientes = Cliente::where('grupo_cliente_id', $id)->get();
        if ($clientes->count())
            return response(["error_msg" => "Existen clientes con este grupo asignado"], 400);

        GrupoCliente::destroy($id);
    }

    public function rules()
    {
        return [
            'nombre' => ['required', 'max:255'],

        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor complete el nombre del grupo de cliente',
            'nombre.max' => 'El nombre del grupo del cliente no puede ser mayo a 255 caracteres',
        ];
    }
}
