<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\GrupoCliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClienteController extends Controller
{

    public function getClientes(Request $request)
    {
        //Devuelvo los clientes con su grupo y todos los grupos para que pueda cargar en el select el frontend
        $data = $request->all();
        $where = null;
        if(!empty($data['filtros']['grupo_cliente_id'])){
            $where = ['grupo_cliente_id' => $data['filtros']['grupo_cliente_id']];
        }
        $clientes = Cliente::with('grupo')->where($where)->get();
        $grupos = GrupoCliente::all();
        return response(['clientes' => $clientes, 'grupos' => $grupos]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cliente = new Cliente();

        $data = $request->all();
        $validator = Validator::make($data, $this->rules(), $this->messages());

        if ($validator->fails())
            return response(["error_msg" => $validator->errors()->first()], 422);

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return response(["error_msg" => 'El formato del email es incorrecto'], 422);
        }


        $cliente->nombre = $data['nombre'];
        $cliente->apellido = $data['apellido'];
        $cliente->email = $data['email'];
        $cliente->grupo_cliente_id = $data['grupo_cliente_id'];
        if (isset($data['observaciones']) && is_string($data['observaciones']))
            $cliente->observaciones = $data['observaciones'];

        $cliente->save();
        $grupos = GrupoCliente::all();
        return response(['cliente' => $cliente, 'grupos' => $grupos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $cliente = Cliente::with('grupo')->find($id);
        if (empty($cliente))
            return response("No se pudo encontrar el cliente solicitado", 400);

        return response($cliente);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $cliente = Cliente::with('grupo')->find($id);
        if (empty($cliente)) {
            return response("El cliente que desea actualizar no existe", 404);
        }

        $data = $request->all();
        $validator = Validator::make($data, $this->rules(), $this->messages());

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return response(["error_msg" => 'El formato del email es incorrecto'], 422);
        }

        if ($validator->fails())
            return response(["error_msg" => $validator->errors()->first()], 422);

        $cliente->nombre = $data['nombre'];
        $cliente->apellido = $data['apellido'];
        $cliente->email = $data['email'];
        $cliente->grupo_cliente_id = $data['grupo_cliente_id'];
        if (isset($data['observaciones']) && is_string($data['observaciones']))
            $cliente->observaciones = $data['observaciones'];
        $cliente->save();

        return response(["cliente" => $cliente]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cliente::destroy($id);
    }

    public function rules()
    {
        return [
            'nombre' => ['required', 'max:255'],
            'apellido' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'observaciones' => ['nullable'],
            'grupo_cliente_id' => ['required', 'numeric'],
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor complete el nombre',
            'nombre.max' => 'El nombre no puede ser mayo a 255 caracteres',
            'apellido.required' => 'Por favor complete el apellido',
            'apellido.max' => 'El apellido no puede ser mayo a 255 caracteres',
            'email.required' => 'Por favor complete el email',
            'email.email' => 'El formato del email es incorrecto',
            'email.max' => 'El email no puede ser mayo a 255 caracteres',
            'grupo_cliente_id.required' => 'Por favor indique el grupo del cliente',
            'grupo_cliente_id.numeric' => 'El formato del grupo del cliente no es válido',
        ];
    }
}
