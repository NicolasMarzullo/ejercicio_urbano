
$(document).ready(function () {

    $('#tableGruposCliente').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "searching": true,
    });

    //Cargo la tabla al inicio
    cargarTablaGrupoClientes();
})


function cargarTablaGrupoClientes() {
    //limpio la tabla
    var tableGruposCliente = $('#tableGruposCliente').DataTable();
    tableGruposCliente.clear().draw()
    $.ajax({
        url: 'gruposClientes',
        type: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (respuesta) {
            
            respuesta.grupos.forEach(grupo => {
                tableGruposCliente.row.add([
                    grupo.nombre,
                    getHtmlAccionesGrupoCliente(grupo)
                ])
            });

            grupos_default = respuesta.grupos
            tableGruposCliente.draw(false)
            actualizarGruposClientesDisponibles(respuesta.grupos)
        },
        error: function (respuesta) {
            toastr.error('Error al obtener los grupos de clientes');
        }
    });
}

//Actualiza los select de crear y editar cliente cuando se edita, crea o elimina un grupo de cliente 
function actualizarGruposClientesDisponibles(grupos) {
    //vacio los select y pongo opcion por defecto
    $('#editar_cliente_grupo').html("<option value='" + -1 + "' selected>Seleccione grupo</option>")
    $('#crear_cliente_grupo').html("<option value='" + -1 + "' selected>Seleccione grupo</option>")
    $('#search_grupo_cliente').html("<option value='" + -1 + "' selected>Seleccione grupo</option>")

    grupos.forEach(grupo => {
        option = "";
        option += "<option value='" + grupo.id + "'>"+ grupo.nombre + "</option>"

        $('#editar_cliente_grupo').append(option)
        $('#crear_cliente_grupo').append(option)
        $('#search_grupo_cliente').append(option)
    });

}

//obtiene el html de las acciones que van en la utlima columna de la tabla de grupo de clientes
function getHtmlAccionesGrupoCliente(grupo) {
    //acciones
    acciones = "";
    acciones += "<td>";
    descripcion = '"' + grupo.nombre + '"';
    acciones += "<button class='btnBorrarGrupoCliente btnFontAwesome' style='color: #690808' onClick='showModalBorrarGrupoCliente(" + grupo.id + ", " + descripcion + ", this)'><i class='fas fa-trash-alt'></i></button>";
    acciones += "<button class='btnEditarGrupoCliente btnFontAwesome' style='color: #0a4b32' onClick='showModalEditarGrupoCliente(" + grupo.id + ", this)'><i class='fas fa-pencil-alt'></i></button>";
    acciones += "</td>";
    acciones += "</tr>";
    return acciones;
}

//Busco el td para luego editar sus datos
deleting_grupo_id = null
function showModalBorrarGrupoCliente(grupo_cliente_id, descripcion, btn) {
    deleting_row = $(btn).parent().parent();
    deleting_grupo_id = grupo_cliente_id;
    $('#tituloBorrarGrupoCliente').html("¿Seguro que desea borrar el grupo de cliente: " + descripcion + "?");
    $('#borrarGrupoClienteModal').modal('show');  //Abro el modal
}

//Metodo llamado por el boton de confirmar borrado del modal de eliminar cliente
$('#btnConfirmarBorradoGrupoCliente').click(function () {
    $.ajax({
        url: 'gruposClientes/' + deleting_grupo_id,
        type: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function () {
            toastr.success('Grupo cliente borrado exitosamente');
            $('#borrarGrupoClienteModal').modal('hide');  //cierro el modal

              //actualizo tabla
              cargarTablaGrupoClientes();         

              //Actualizo la tabla de clientes para que se vean los select actualizados
              cargarTablaClientes()
        },
        error: function (respuesta) {
            toastr.error('No se ha podido borrar al cliente: ' + respuesta.responseJSON.error_msg);
        }
    });
})

//Actualiza el grupo del cliente desde el listado
function actualizarGrupoCliente(select, cliente_id) {
    grupo_seleccionado = $(select).val()
    cliente = {
        'id': cliente_id,
        'nombre': $(select).parent().siblings()[0].innerText,
        'apellido': $(select).parent().siblings()[1].innerText,
        'email': $(select).parent().siblings()[2].innerText,
        'observaciones': null,
        'grupo_cliente_id': grupo_seleccionado
    }
    actualizarGrupoCliente(cliente);
};

//Peticiona al backend para actualizar 
function actualizarGrupoCliente(grupo) {
    $.ajax({
        url: 'gruposClientes/' + grupo.id,
        type: 'PUT',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': cliente.id,
            'nombre': cliente.nombre,
            'apellido': cliente.apellido,
            'email': cliente.email,
            'observaciones': cliente.observaciones,
            'grupo_cliente_id': cliente.grupo_cliente_id
        },
        success: function (respuesta) {
            toastr.success('Grupo de cliente actualizado');
        },
        error: function (respuesta) {
            toastr.error('No se ha podido actualizar el grupo del cliente: ' + respuesta.error_msg);
        }
    });
}

//Evento que se ejecuta cuando se le hace click a crear dentro del modal de crear grupo cliente
$("#btnCrearGrupoCliente").click(function () {
    grupo_cliente = {
        'id': null,
        'nombre': $("#crear_grupo_cliente_nombre").val(),
    }
    crearGrupoCliente(grupo_cliente);
});

//Peticiona al backend para crear un nuevo grupo cliente
function crearGrupoCliente(grupo_cliente) {

    $.ajax({
        url: 'gruposClientes',
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        data: {
            'id': null,
            'nombre': grupo_cliente.nombre,
        },
        success: function (respuesta) {
            toastr.success('Grupo cliente creado correctamente');
            $('#crearGrupoClienteModal').modal('hide');  //cierro el modal 
              //actualizo tabla
              cargarTablaGrupoClientes();         

              //Actualizo la tabla de clientes para que se vean los select actualizados
              cargarTablaClientes()
            

            
        },
        error: function (respuesta) {
            toastr.error('Error al crear cliente: ' + respuesta.responseJSON.error_msg)
        }
    });
}

editing_row = null;
function showModalEditarGrupoCliente(grupo_id, btn) {

    //Busco el td para luego editar sus datos
    editing_row = $(btn).parent().parent();
    //Busco loas datos del cliente solicitado
    $.ajax({
        url: 'gruposClientes/' + grupo_id,
        type: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (respuesta) {
            //Seteo el resultado que recibí de backend     
            $('#editar_grupo_cliente_id').val(respuesta.id)
            $("#editar_grupo_cliente_nombre").val(respuesta.nombre)
            $('#editarGrupoClienteModal').modal('show');  //Abro el modal
        },
        error: function (respuesta) {
            toastr.error(respuesta);
        }
    });
}

//Evento que se ejecuta cuando se le hace click a guardar cambios dentro del modal de editar cliente
$("#btnEditarGrupoCliente").click(function () {
    grupo_cliente = {
        'id': $("#editar_grupo_cliente_id").val(),
        'nombre': $("#editar_grupo_cliente_nombre").val(),
    }
    editarGrupoCliente(grupo_cliente);
});


//Peticiona al backend para editar un cliente
function editarGrupoCliente(grupo_cliente) {

    $.ajax({
        url: 'gruposClientes/' + grupo_cliente.id,
        type: 'PUT',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': grupo_cliente.id,
            'nombre': grupo_cliente.nombre
        },
        success: function (respuesta) {
            toastr.success('Grupo cliente editado correctamente');
            $('#editarGrupoClienteModal').modal('hide');  //Abro el modal            
            //actualizo tabla
            cargarTablaGrupoClientes();         

            //Actualizo la tabla de clientes para que se vean los select actualizados
            cargarTablaClientes()
        },
        error: function (respuesta) {
            toastr.error('Error al editar el grupo cliente: ' + respuesta.responseJSON.error_msg)
        }
    });
}