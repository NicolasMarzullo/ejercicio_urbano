
$(document).ready(function () {

    $('#tableClientes').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        //"searching": false,
        //"fixedHeader": true,
        "columns": [
            { "orderable": true },
            { "orderable": true },
            { "orderable": true },
            { "orderable": false },
            { "orderable": false }
        ]
    });

    //Cargo la tabla al inicio
    cargarTablaClientes();

    //Bind de los input para filtrar la tabla de cleintes
    var tableClientes = $('#tableClientes').DataTable();
    $('#search_nombre').on('keyup change', function () {
        if (tableClientes.column(0).search() !== this.value) {
            tableClientes
                .column(0)
                .search(this.value)
                .draw();
        }
    });

    $('#search_apellido').on('keyup change', function () {
        if (tableClientes.column(1).search() !== this.value) {
            tableClientes
                .column(1)
                .search(this.value)
                .draw();
        }
    });

    $('#search_email').on('keyup change', function () {
        if (tableClientes.column(2).search() !== this.value) {
            tableClientes
                .column(2)
                .search(this.value)
                .draw();
        }
    });

    //Cuando cambia el search por grupo de cliente hago un get y el backend hace el filtro
    $('#search_grupo_cliente').on('change', function () {
        filtros = null;
        //Solo si es mayor a 0, el -1 lo uso para deshabilitar el filtro
        console.log(this.value);
        if (parseInt(this.value) > 0){
            filtros = {
                "grupo_cliente_id": this.value
            }
        }
        cargarTablaClientes(filtros)
    });
})

let grupos_default = "";
function cargarTablaClientes(filtros) {
    //limpio la tabla
    var tableClientes = $('#tableClientes').DataTable();
    tableClientes.clear().draw();

    $.ajax({
        url: 'api/clientes',
        type: 'GET',
        dataType: 'json',
        data: {
            "filtros": filtros ? filtros : null
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (respuestaClientes) {

            //Pido los grupos de clientes para llenar los select
            $.ajax({
                url: 'gruposClientes',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (respuestaGrupoCliente) {
                    grupos_default = respuestaGrupoCliente.grupos;
                    respuestaClientes.clientes.forEach(cliente => {
                        tableClientes.row.add([
                            cliente.nombre,
                            cliente.apellido,
                            cliente.email,
                            getHtmlSelectGrupo(cliente, grupos_default),
                            getHtmlAccionesCliente(cliente)
                        ])
                    });

                    tableClientes.draw(false)
                },
                error: function (respuesta) {
                    toastr.error('Error al obtener los grupos de clientes');
                }
            });
        },
        error: function (respuesta) {
            toastr.error('Error al obtener los clientes');
        }
    });
}

//Obtiene el html del select que va en la tabla de clientes
function getHtmlSelectGrupo(cliente, grupos) {
    select = ""
    select += "<select onChange='actualizarSelectGrupoCliente(this, " + cliente.id + ")'>"

    grupos.forEach(grupo => {
        selected = "";
        if (cliente.grupo_cliente_id == grupo.id)
            selected = "selected";
        select += "<option value='" + grupo.id + "' " + selected + " >" + grupo.nombre + "</option>"
    });
    select += "</select>"

    return select
}

//obtiene el html de las acciones que van en la utlima columna de la tabla ed clientes
function getHtmlAccionesCliente(cliente) {
    //acciones
    acciones = "";
    acciones += "<td>";
    nombre_completo = '"' + cliente.nombre + ', ' + cliente.apellido + '"';
    acciones += "<button class='btnBorrarCliente btnFontAwesome' style='color: #690808' onClick='showModalBorrarCliente(" + cliente.id + ", " + nombre_completo + ", this)'><i class='fas fa-trash-alt'></i></button>";
    acciones += "<button class='btnEditarCliente btnFontAwesome' style='color: #0a4b32' onClick='showModalEditarCliente(" + cliente.id + ", this)'><i class='fas fa-pencil-alt'></i></button>";
    acciones += "</td>";

    acciones += "</tr>";
    return acciones;
}

//Busco el td para luego editar sus datos
let deleting_row = null
let deleting_client_id = null
function showModalBorrarCliente(cliente_id, descripcion, btn) {
    deleting_row = $(btn).parent().parent();
    deleting_client_id = cliente_id;
    $('#tituloBorarClienteConfirmacion').html("¿Seguro que desea borrar el cliente: " + descripcion + "?");
    $('#borrarClienteModal').modal('show');  //Abro el modal
}

//Metodo llamado por el boton de confirmar borrado del modal de eliminar cliente
$('#btnConfirmarBorradoCliente').click(function () {
    $.ajax({
        url: 'clientes/' + deleting_client_id,
        type: 'DELETE',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function () {
            $(deleting_row).remove();
            toastr.success('Cliente borrado exitosamente');
            $('#borrarClienteModal').modal('hide');  //cierro el modal
        },
        error: function () {
            toastr.error('No se ha podido borrar al cliente');
        }
    });
})

//Actualiza el grupo del cliente desde el listado
function actualizarSelectGrupoCliente(select, cliente_id) {
    grupo_seleccionado = $(select).val()
    cliente = {
        'id': cliente_id,
        'nombre': $(select).parent().siblings()[0].innerText,
        'apellido': $(select).parent().siblings()[1].innerText,
        'email': $(select).parent().siblings()[2].innerText,
        'observaciones': null,
        'grupo_cliente_id': grupo_seleccionado
    }
    actualizarCliente(cliente);
};

//Peticiona al backend para actualizar el cliente
function actualizarCliente(cliente) {
    $.ajax({
        url: 'clientes/' + cliente.id,
        type: 'PUT',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': cliente.id,
            'nombre': cliente.nombre,
            'apellido': cliente.apellido,
            'email': cliente.email,
            'observaciones': cliente.observaciones,
            'grupo_cliente_id': cliente.grupo_cliente_id
        },
        success: function (respuesta) {
            toastr.success('Cliente actualizado');
        },
        error: function (respuesta) {
            toastr.error('No se ha podido actualizar el cliente: ' + respuesta.error_msg);
        }
    });
}

//Evento que se ejecuta cuando se le hace click a crear dentro del modal de crear cliente
$("#btnCrearCliente").click(function () {
    cliente = {
        'id': null,
        'nombre': $("#crear_cliente_nombre").val(),
        'apellido': $("#crear_cliente_apellido").val(),
        'email': $("#crear_cliente_email").val(),
        'observaciones': $("#crear_cliente_observaciones").val(),
        'grupo_cliente_id': $("#crear_cliente_grupo").val(),
    }
    crearCliente(cliente);
});

//Peticiona al backend para crear un nuevo cliente
function crearCliente(cliente) {

    $.ajax({
        url: 'clientes',
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: "json",
        data: {
            'id': cliente.id,
            'nombre': cliente.nombre,
            'apellido': cliente.apellido,
            'email': cliente.email,
            'observaciones': cliente.observaciones,
            'grupo_cliente_id': cliente.grupo_cliente_id
        },
        success: function (respuesta) {
            toastr.success('Cliente creado correctamente');
            $('#crearClienteModal').modal('hide');  //cierro el modal 
            //actualizo tabla
            var tableClientes = $('#tableClientes').DataTable();

            tableClientes.row.add([
                respuesta.cliente.nombre,
                respuesta.cliente.apellido,
                respuesta.cliente.email,
                getHtmlSelectGrupo(respuesta.cliente, grupos_default),
                getHtmlAccionesCliente(respuesta.cliente)
            ])

            tableClientes.draw(false)
        },
        error: function (respuesta) {
            toastr.error('Error al crear cliente: ' + respuesta.responseJSON.error_msg)
        }
    });
}

let editing_row = null;
function showModalEditarCliente(cliente_id, btn) {

    //Busco el td para luego editar sus datos
    editing_row = $(btn).parent().parent();
    //Busco loas datos del cliente solicitado
    $.ajax({
        url: 'clientes/' + cliente_id,
        type: 'GET',
        dataType: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (respuesta) {
            //Seteo el resultado que recibí de backend            
            $("#editar_cliente_nombre").val(respuesta.nombre)
            $("#editar_cliente_apellido").val(respuesta.apellido)
            $("#editar_cliente_email").val(respuesta.email)
            $("#editar_cliente_observaciones").val(respuesta.observaciones)
            $("#editar_cliente_grupo").val(respuesta.grupo.id)
            $('#editar_cliente_id').val(cliente_id)
            $('#editarClienteModal').modal('show');  //Abro el modal
        },
        error: function (respuesta) {
            toastr.error(respuesta);
        }
    });
}

//Evento que se ejecuta cuando se le hace click a guardar cambios dentro del modal de editar cliente
$("#btnEditarCliente").click(function () {
    cliente = {
        'id': $("#editar_cliente_id").val(),
        'nombre': $("#editar_cliente_nombre").val(),
        'apellido': $("#editar_cliente_apellido").val(),
        'email': $("#editar_cliente_email").val(),
        'observaciones': $("#editar_cliente_observaciones").val(),
        'grupo_cliente_id': $("#editar_cliente_grupo").val(),
    }
    editarCliente(cliente);
});


//Peticiona al backend para editar un cliente
function editarCliente(cliente) {

    $.ajax({
        url: 'clientes/' + cliente.id,
        type: 'PUT',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            'id': cliente.id,
            'nombre': cliente.nombre,
            'apellido': cliente.apellido,
            'email': cliente.email,
            'observaciones': cliente.observaciones,
            'grupo_cliente_id': cliente.grupo_cliente_id
        },
        success: function (respuesta) {
            toastr.success('Cliente editado correctamente');
            $('#editarClienteModal').modal('hide');  //cierro el modal 
            //actualizo tabla
            $(editing_row).children()[0].innerText = respuesta.cliente.nombre // nombre
            $(editing_row).children()[1].innerText = respuesta.cliente.apellido  // apellido
            $(editing_row).children()[2].innerText = respuesta.cliente.email  // email
            td = $(editing_row).children()[3];
            $(td).children().val(respuesta.cliente.grupo_cliente_id) // select grupo
        },
        error: function (respuesta) {
            toastr.error('Error al editar cliente: ' + respuesta.responseJSON.error_msg)
        }
    });
}